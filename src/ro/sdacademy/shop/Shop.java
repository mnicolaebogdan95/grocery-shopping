package ro.sdacademy.shop;

import ro.sdacademy.customer.Customer;
import ro.sdacademy.product.Product;
import java.util.Scanner;

public class Shop {
    int nrOfProducts;
    Product[] products;
    Customer customer;

    public Shop(int nrOfProducts, Customer customer) {
        this.nrOfProducts = nrOfProducts;
        products = new Product[nrOfProducts];
        this.customer = customer;
    }

    public void populateProducts(Scanner scanner) {
        System.out.println("Enter the products you want to sell: ");
        for (int i = 0; i < products.length; i++) {
            System.out.println("Enter product " + (i + 1) + ": ");
            System.out.print("Enter name:");
            scanner.nextLine();
            String name = scanner.nextLine();
            System.out.print("Enter price:");
            double price = scanner.nextDouble();
            Product product = new Product(name, price);
            products[i] = product;
        }
    }

    public void chooseProduct(Scanner scanner) {
        int userOption;
        String answer = "";
        System.out.println("You could order maximum " + customer.nrMaxOfProductsInCart + " products.");
        do {
            int remainingProductsToBuy = customer.nrMaxOfProductsInCart - customer.noOfProductsInCart();
            System.out.println("You could buy " + remainingProductsToBuy + " more products.");
            do {
                System.out.print("Please choose a valid product: ");
                userOption = scanner.nextInt();
                scanner.nextLine();
            } while (userOption < 1 || userOption > products.length);
            if (customer.isCartFull()) {
                System.out.println("Your cart is full! Please check out and pay.");
                checkOutAndPay();
                return;
            }else {
                customer.addProductToCart(userOption, products);
            }
            if (!customer.isCartFull()){
                System.out.print("Do you want to buy more products?(Y/N) ");
                answer = scanner.nextLine();
            }else {
                System.out.println("Your cart is full! Please check out and pay.");
                checkOutAndPay();
                return;
            }
        } while (!customer.isCartFull() && answer.equalsIgnoreCase("y"));
    }

    public void checkOutAndPay() {
        customer.showCart();
        customer.calculateTotalCost();
        System.out.println("You have to pay " + customer.cartValue + " RON");
        System.out.println("Paid successfully!");
        System.out.println("Thank you for buying from us! Now you could do another order!");
        customer.clearCart();
    }


    public void showMenuOfProducts(Scanner scanner) {
        System.out.println("The product menu is: ");
        for (int i = 0; i < products.length; i++) {
            System.out.println(i + 1 + ". " + "Product Name: " + products[i].name + " | Price: " + products[i].price + " RON");
        }
        chooseProduct(scanner);
    }

    public void startShopping() {
        chooseOption();
    }

    public void chooseOption() {
        Scanner scanner = new Scanner(System.in);
        int userSelectedOption;
        String userAnswer;
        do {
            showMenu();
            do {
                System.out.print("Choose an option: ");
                userSelectedOption = scanner.nextInt();
                switch (userSelectedOption) {
                    case 1:
                        if (customer.isCartFull()) {
                            System.out.println("You cart is full of product!");
                        } else {
                            showMenuOfProducts(scanner);
                        }
                        break;
                    case 2:
                        customer.showCart();
                        break;
                    case 3:
                        customer.showCartValue();
                        break;
                    case 4:
                        checkOutAndPay();
                        break;
                    case 5:
                        scanner.nextLine();
                        leaveShop(scanner);
                        return;
                    default:
                        System.out.print("Please choose a valid option: ");
                }
            } while ((userSelectedOption < 1 || userSelectedOption > 5));
            System.out.println("Do you want to choose another menu option? (y/n)");
            Scanner scannerString = new Scanner(System.in);
            userAnswer = scannerString.nextLine();
            if (userAnswer.equalsIgnoreCase("n")) {
                System.out.println("Thanks for buying from us!");
                System.out.println("Good bye! :)");
            }
        } while (userAnswer.equalsIgnoreCase("y"));

    }

    public void leaveShop(Scanner scanner) {
        System.out.print("Are you sure you want to leave the shop? (y/n) ");
        String userAnswer = scanner.nextLine();
        if (userAnswer.equalsIgnoreCase("y")) {
            goodByeMessage();
        } else {
            startShopping();
        }
    }

    public void showMenu() {
        System.out.println("1. " + "Show menu of products");
        System.out.println("2. " + "Show cart");
        System.out.println("3. " + "Show total price");
        System.out.println("4. " + "Checkout and pay");
        System.out.println("5. " + "Leave the shop");
    }

    public void goodByeMessage(){
        System.out.println("Thanks for buying from us!");
        System.out.println("Good bye! :)");
    }
}
