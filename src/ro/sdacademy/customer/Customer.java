package ro.sdacademy.customer;

import ro.sdacademy.product.Product;

public class Customer {
    public int nrMaxOfProductsInCart;
    public Product[] cartList;
    public double cartValue;

    public Customer(int nrOfProducts) {
        this.nrMaxOfProductsInCart = nrOfProducts;
        cartList = new Product[nrOfProducts];
        System.out.println("TEST");
        System.out.println("TEST");
        System.out.println("TEST");
        System.out.println("TEST");
    }

    public void showCart() {
        if (!isCartEmpty()) {
            System.out.println("Your cart have the following products:");
            for (int i = 0; i < cartList.length; i++) {
                if (cartList[i] != null) {
                    System.out.println(i + 1 + ". " + "Product Name: " + cartList[i].name + " | Price: " + cartList[i].price + " RON");
                }
            }
        } else {
            System.out.println("Your cart is empty! :)");
        }
    }

    public void calculateTotalCost() {
        if (isCartEmpty()) {
            return;
        }
        for (int i = 0; i < cartList.length; i++) {
            if (cartList[i] != null){
                cartValue += cartList[i].price;
            }
        }
    }

    public void showCartValue() {
        double cartVal = cartValue;
        calculateTotalCost();
        System.out.println("You have to pay " + cartValue + " RON");
        cartValue = cartVal;
    }

    public boolean isCartEmpty() {
        boolean isEmpty = true;
        for (int i = 0; i < cartList.length; i++) {
            if (cartList[i] != null) {
                isEmpty = false;
            }
        }
        return isEmpty;
    }

    public boolean isCartFull() {
        boolean isCartFull = false;
        int contor = 0;

        for (int i = 0; i < cartList.length; i++) {
            if (cartList[i] != null) {
                contor++;
            }
        }
        if (contor == cartList.length) {
            isCartFull = true;
        }
        return isCartFull;
    }

    public void clearCart() {
        for (int i = 0; i < cartList.length; i++) {
            cartList[i] = null;
        }
        cartValue = 0.0;
    }

    public void addProductToCart(int userOption, Product[] products) {
        int firstEmptyIndex = findFirstEmptyIndexInCart();
        cartList[firstEmptyIndex] = products[userOption - 1];
        System.out.println("You successful added " + products[userOption - 1].name + " to your cart.");
    }

    public int findFirstEmptyIndexInCart() {
        int emptyIndex = 0;
        if (isCartEmpty()) {
            return emptyIndex;
        } else {
            for (int i = 0; i < cartList.length; i++) {
                if (cartList[i] == null){
                    emptyIndex = i;
                }
            }
        }
        return emptyIndex;
    }

    public int noOfProductsInCart(){
        int noOfProductsInCart = 0;
        for (int i = 0; i < cartList.length; i++) {
            if (cartList[i] != null){
                noOfProductsInCart++;
            }
        }
        return noOfProductsInCart;
    }
}
